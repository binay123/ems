<?php

/**
 * Allows php class files to be included automatically when constructing a class.
 *
 * @param   string   $class_name The name of the class to be included (excluding .class.php)
 */

 spl_autoload_register(function ($class_name){
	$class_path = CLASSES. '/' . $class_name . '.class.php';
	if(file_exists($class_path)){
		require_once $class_path;
	}else{
		throw new Exception("Failed to include class: ", $class_name);		
	}
});


?>