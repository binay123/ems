<?php

define('ROOT', dirname(dirname(__FILE__)));
define('SYSTEM', ROOT . '/system');
define('CLASSES', SYSTEM . '/classes');

define('VIEWS', ROOT . '/views');

define('BASE_URL','http://localhost/ems' );

//database 
define('DB_HOST', 'localhost');
define('DB_NAME', 'ems');
define('DB_USER', 'root');
define('DB_PASS', '');

//school address
define('SCHOOL_ADDRESS', 'Hurstville Public School, Forest Rd, Hurstville NSW 2220');
define('SCHOOL_LATITUDE', '-33.96468475018911');
define('SCHOOL_LONGITUDE', '151.11113022704933');

?>