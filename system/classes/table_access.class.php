<?php
/**
 * 	Table Access Class
 *
 *  This class allows basic Add/Edit/Delete functions for any generic table
 *
 */

class table_access extends db{

    protected $table_name;

    /**
     * Set Table Name
     *
     * @param   string   $table_name   The database table name
     */
    public function set_table($table_name){
    	$this->table_name = $table_name;
    }

    /**
     * Fetch data by id
     *
     * @param   int   $id   The id to be used for fetch
     * @return  array       The array of fetched value
     */
    public function fetch_by_id($id){
        $sql = "SELECT * FROM $this->table_name WHERE id='$id'";
        return(isset($this->fetch_query($sql)[0]) ? $this->fetch_query($sql)[0] : '' );
    } 


    /**
     * Fetch data based on given condition
     *
     * @param   array   $relation   Optional. The array with table joins and columns to be fetched 
     * @param   array   $condition  Optional. The array with conditions to be used in fetch 
     * @param   string  $sort_by    Optional. The column by which the data is to be sorted
     * @return  array               The array of fetched value
     */
    public function fetch_all($relation = [], $condition = [], $sort_by = '')    {
        if(!empty($relation)){

            $sql = "SELECT $this->table_name.*, " . $relation['fetch_column'];            

            $sql .= "$this->table_name.id FROM $this->table_name ";

            $sql .= $relation['join'];

            $sql .= " WHERE $this->table_name.flag = 1";


        }else{
            $sql = "SELECT * FROM $this->table_name WHERE flag = 1";
        } 

        if(!empty($condition)){
            foreach ($condition as $key => $value) {
                switch ($key) {
                    case 'where_in':
                        foreach ($value as $c) {
                            $sql .= " AND " . $c['column'] . " IN (" . $c['value'] . ")";
                        }
                        
                        break;

                    case 'where':
                        foreach ($value as $c) {
                            $v = $c['value'];
                            $sql .= " AND " . $c['column'] . " = '$v' " ;
                        }
                        
                        break;

                    case 'where_min':
                        foreach ($value as $c) {
                            $v = $c['value'];
                            $sql .= " AND " . $c['column'] . " >= '$v' " ;
                        }
                        
                        break;

                    case 'where_max':
                        foreach ($value as $c) {
                            $v = $c['value'];
                            $sql .= " AND " . $c['column'] . " <= '$v' " ;
                        }
                        
                        break;
                    
                    default:
                        
                        break;
                }
            }
        } 

        if($sort_by != ''){
            $sql .= " ORDER BY " . $sort_by . " ASC";
        } 

        return($this->fetch_query($sql));
    }

    /**
     * Insert into database table
     *
     * @param   array   $data   The array of data to be inserted
     * @return  bool            The result of insert into database table
     */
    public function insert($data){
        $column = '';
        $values = '';
        foreach($data as $key => $value){
            if(!empty($value)) {
                if(is_string($value)) {
                    $value = $this->sanitize_data($value);
                    $values .= "'$value'" . ",";
                } else {
                    $values .= $value . ",";
                }
                $column .= $key . ",";
            }
        }
        $column = rtrim($column,',');
        $values = rtrim($values,',');
        $sql = "INSERT INTO  $this->table_name ( $column ) VALUES ( $values );";
        if($this->execute_query($sql)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update values in database table
     *
     * @param   array   $data      The array of data to be updated
     * @param   string  $column    The column to be used for comparision
     * @param   string  $condition The value to be compared
     * @return  bool               The result of database table update
     */
    public function update($data, $column, $condition){
        $phrase = '';
        foreach($data as $key => $value){
            if(!empty($value)) {
                if(is_string($value)) {
                    $value = $this->sanitize_data($value);
                    $phrase .= $key . "= '$value'" . ",";
                } else {
                    $phrase .= $key . "=" .$value . ",";
                }
            }
        }
        $phrase = rtrim($phrase,',');
        $sql = "UPDATE $this->table_name SET $phrase WHERE $column = $condition;";

        if($this->execute_query($sql)){
            return true;
        }else{
            return false;
        }

    }

    /**
     * Soft delete update flag to 0
     *
     * @param   int   $id      The id of data to be deleted
     * @return  bool           The result of delete
     */
    public function delete($id){
        $sql = "UPDATE $this->table_name SET flag = 0 WHERE id = $id";

        if($this->execute_query($sql)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Sanitize the string: removes whitespace, removes backslashes and Convert all applicable characters to HTML entities
     *
     * @param   string   $input  The string to be sanitize
     * @return  string           The sanitized string
     */
    public function sanitize_data($input) {
        $output = trim($input);
        $output = stripslashes($output);
        $output = htmlentities($output);
        return $output;
    }

}