<?php

/**
 *  Organizer Class
 *
 *  This is controller class for Organizer
 */

class organizer extends table_access{

	public function __construct(){

		$this->set_table('organizer');

	}

	/**
     * Fetch the organizer data 
     * @return  array                The fetched array of organizers
     */
	public function index(){
		$organizers = $this->fetch_all();
		return $organizers;
	}

	/**
     * Create new organizer
     *
     * @param   array   $input   The organizer array
     * @return  bool             The result of insert into database table
     */
	public function create($input){
		$input['created_date'] = date("Y-m-d H:i:s");
		$input['updated_date'] = date("Y-m-d H:i:s");
		$input['flag'] = 1;
		$create = $this->insert($input);
		return $create;
	}

	/**
     * Update organizer 
     *
     * @param   array   $input   The organizer array with new updated values
     * @param   int     $id      The organizer id to be updated
     * @return  bool             The result of organizer update
     */
	public function edit($input, $id){
		$input['updated_date'] = date("Y-m-d H:i:s");		
		$update = $this->update($input, 'id', $id);
		return $update;
	}
}