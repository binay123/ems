<?php

/**
 * This class sends the api request
 */


abstract class api{

	protected $api_server;
	protected $api_key;
	protected $api_url;

	/**
     * Set API details based on action to be performed     
     *
     */
	abstract public function set_api_details();	

	/**
     * Send API request     
     *
     * @return  string					The response from the API
     */

	public function send_api_request(){			
		try{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->api_url);						
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);						

			$response = curl_exec ($ch);
			if ($response === false) {
                throw new Exception(curl_error($ch), curl_errno($ch)); 
            }			
		}catch (Exception $e) {
            $error_message = $e->getMessage();
        }

        curl_close($ch);
        return $response;
	}	
}