<?php

/**
 *  Database Class
 *
 *  This class performs the database configuration
 */

abstract class db{

    private $host = DB_HOST;
    private $db_name = DB_NAME;
    private $username = DB_USER;
    private $password = DB_PASS;


    /**
     * Creates a PDO instance representing a connection to a database
     *     
     * @return  object                     The PDO database connection object
     */
    private function get_connection(){

        $connection = null;
        try{
            $connection = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $connection;
    }


    /**
     * Executes the query string     
     *
     * @param   string   $sql          The sql string to be executed
     * @return  bool                   True on success and False on Failure
     */
    protected function execute_query($sql){
        $connection = $this->get_connection();
        $statement = $connection->prepare($sql);
        $execute = $statement->execute();
        return $execute;
    }


    /**
     * Fetch the data from the database table     
     *
     * @param   string   $sql          The sql string to be executed
     * @return  array                  The array of data fetched from the database table
     */
    protected function fetch_query($sql){
        $connection = $this->get_connection();
        return $connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }
}

?>