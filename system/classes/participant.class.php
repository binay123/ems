<?php

/**
 *  Participant Class
 *
 *  This is controller class for Participants
 */

class participant extends table_access{

	public function __construct(){

		$this->set_table('participant');

	}

	/**
     * Fetch the participant data 
     *
     * @param   array   $condition   Optional. The array of any condition to be used for fetch
     * @return  array                The fetched array of participants
     */
	public function index($condition=[]){
		$relation['fetch_column'] = 'participant_group.*, ';
		$relation['join'] = 'INNER JOIN participant_group ON participant.group_id = participant_group.id';
		$participants = $this->fetch_all($relation, $condition);
		return $participants;
	}

	/**
     * Create new participant
     *
     * @param   array   $input   The participant array
     * @return  bool             The result of insert into database table
     */
	public function create($input){
		$input['created_date'] = date("Y-m-d H:i:s");
		$input['updated_date'] = date("Y-m-d H:i:s");
		$input['flag'] = 1;
		$create = $this->insert($input);
		return $create;
	}

	/**
     * Update participant 
     *
     * @param   array   $input   The participant array with new updated values
     * @param   int     $id      The participant id to be updated
     * @return  bool             The result of participant update
     */
	public function edit($input, $id){
		$input['updated_date'] = date("Y-m-d H:i:s");		
		$update = $this->update($input, 'id', $id);
		return $update;
	}
}

?>