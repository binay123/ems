<?php

/**
 *  Payment Class
 *
 *  This is controller class for Participants who have paid for the events
 */

class payment extends table_access{

	public function __construct(){

		$this->set_table('payment');

	}

	/**
     * Create new payment
     *
     * @param   array   $input   The payment array
     * @return  bool             The result of insert into database table
     */
	public function create($input){
		$input['created_date'] = date("Y-m-d H:i:s");
		$input['flag'] = 1;
		$create = $this->insert($input);
		return $create;
	}

	/**
     * Get attendees detail for an event
     *
     * @param   int   $event_id  The event id for with the attendees are to be fetched
     * @return  array            The array of attendees of the event
     */
	public function get_attendees($event_id){
		$relation['fetch_column'] = 'participant.first_name, participant.last_name, participant_group.group_name, ';
		$relation['join'] = 'INNER JOIN participant ON participant.id = payment.participant_id INNER JOIN participant_group ON participant.group_id = participant_group.id';
		$condition = ['where' => [['column' => 'event_id', 'value' => $event_id]]];
		$attendees = $this->fetch_all($relation, $condition);
		return $attendees;
	}
}

?>