<?php
/**
 *  Participant Group Class
 *
 *  This is controller class for Participant Groups
 */

class participant_group extends table_access{

	public function __construct(){

		$this->set_table('participant_group');

	}

	/**
     * Fetch the participant group data 
     * @return  array                The fetched array of participant group
     */
	public function index(){
		$participant_groups = $this->fetch_all();
		return $participant_groups;
	}

	/**
     * Create new participant group
     *
     * @param   array   $input   The participant group array
     * @return  bool             The result of insert into database table
     */
	public function create($input){
		$input['created_date'] = date("Y-m-d H:i:s");
		$input['updated_date'] = date("Y-m-d H:i:s");
		$input['flag'] = 1;
		$create = $this->insert($input);
		return $create;
	}

	/**
     * Update participant group 
     *
     * @param   array   $input   The participant group array with new updated values
     * @param   int     $id      The participant group id to be updated
     * @return  bool             The result of participant group update
     */
	public function edit($input, $id){
		$input['updated_date'] = date("Y-m-d H:i:s");		
		$update = $this->update($input, 'id', $id);
		return $update;
	}
}