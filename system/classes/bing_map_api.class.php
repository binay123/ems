<?php
/**
 * Bing Map api controller
 */

class bing_map_api extends api{	

	public function __construct(){
		$this->set_api_details();
	}

	/**
     * Set API server and api key for bing map api    
     *
     */
	public function set_api_details(){
		$this->api_server = 'http://dev.virtualearth.net/REST/v1/';
		$this->api_key = 'AjeNvFZblo9StP4rHbyLBjG7layBFY-__4oymRWFz9Qm_jFFKPDMqNbMIa1fj3jF';
	}

	/**
     * Get the longitude and latitude coordinate points for given address   
     *
     * @param   string   $address      The address
     * @return  array                  The location array with longitude and latitude coordinate points
     */
	public function get_location_by_address($address){
		$this->api_url = $this->api_server . 'Locations/' . $address . '?o=json&key=' . $this->api_key;

		$result = $this->send_api_request();

		$result_array = json_decode($result, true);

		$location['lat'] = '';
		$location['long'] = '';

		if($result_array['statusCode'] == 200){

			$location['lat'] = isset($result_array['resourceSets'][0]['resources'][0]['point']['coordinates'][0]) ? $result_array['resourceSets'][0]['resources'][0]['point']['coordinates'][0] : '';
			$location['long'] = isset($result_array['resourceSets'][0]['resources'][0]['point']['coordinates'][1]) ? $result_array['resourceSets'][0]['resources'][0]['point']['coordinates'][1] : '';
		}

		//echo '<pre>'.var_export($location, true) .'</pre>';

		return $location;
		
	}


	/**
     * Get the distance and travel time between two address   
     *
     * @param   array   $origin      The array with lat and long coordinate points of origin
     * @param   array   $destination The array with lat and long coordinate points of destination
     * @return  array                The array with distance and trvel time between origin and destination
     */
	public function get_distance($origin, $destination){
		$this->api_url = $this->api_server . 'Routes/DistanceMatrix?origins=' . $origin['lat'] . ',' . $origin['long'] . '&destinations=' . $destination['lat'] . ',' . $destination['long'] . '&travelMode=driving&key=' . $this->api_key;

		$result = $this->send_api_request();

		$result_array = json_decode($result, true);

		$distance['distance'] = '';
		$distance['travel_time'] = '';

		if($result_array['statusCode'] == 200){

			$distance['distance'] = isset($result_array['resourceSets'][0]['resources'][0]['results'][0]['travelDistance']) ? $result_array['resourceSets'][0]['resources'][0]['results'][0]['travelDistance'] : '';
			$distance['travel_time'] = isset($result_array['resourceSets'][0]['resources'][0]['results'][0]['travelDuration']) ? $result_array['resourceSets'][0]['resources'][0]['results'][0]['travelDuration'] : '';
		}


		//echo '<pre>'.var_export($distance, true) .'</pre>';

		return $distance;
	}
	
}