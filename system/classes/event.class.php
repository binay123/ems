<?php

/**
 *  Event Class
 *
 *  This is controller class for events
 */

class event extends table_access{

	public function __construct(){

		$this->set_table('event');

	}

	/**
     * Fetch the event data 
     *
     * @param   array   $condition   Optional. The array of any condition to be used for fetch
     * @param   string  $sort_by     OPtional. The column value by which the result needs to be searched
     * @return  array                The fetched array of events
     */
	public function index($condition = [], $sort_by = ''){
		$event = $this->fetch_all([], $condition, $sort_by);
		return $event;
	}

	/**
     * Create new event
     *
     * @param   array   $input   The event array
     * @return  bool             The result of insert into database table
     */
	public function create($input){
		$input['event_organizer'] = implode(',', $input['event_organizer']);
		$input['event_participant'] = implode(',', $input['event_participant']);

		$bing_map_api = new bing_map_api();

		$address = str_replace(' ', '%20' ,'AU/' . $input['state'] . '/' . $input['postcode'] . '/' . $input['suburb'] . '/' . $input['line1']);

		$location = $bing_map_api->get_location_by_address($address);

		$input['event_venue'] = $input['line1'] . ', ' . $input['suburb'] . ', ' . $input['postcode'] . ', ' . $input['state'] . ', AU' ; 


		if($location['lat'] != '' && $location['long'] != ''){
			$distance = $bing_map_api->get_distance(['lat' => SCHOOL_LATITUDE, 'long' => SCHOOL_LONGITUDE], $location);
			$input['distance_from_school'] = $distance['distance'];
			$input['travel_time'] = $distance['travel_time'];
		}	
		

		
		$input['created_date'] = date("Y-m-d H:i:s");
		$input['updated_date'] = date("Y-m-d H:i:s");
		$input['flag'] = 1;
		$create = $this->insert($input);
		return $create;
	}


	/**
     * Update event 
     *
     * @param   array   $input   The event array with new updated values
     * @param   int     $id      The event id to be updated
     * @return  bool             The result of event update
     */
	public function edit($input, $id){
		$input['event_organizer'] = implode(',', $input['event_organizer']);
		$input['event_participant'] = implode(',', $input['event_participant']);

		$bing_map_api = new bing_map_api();

		$address = str_replace(' ', '%20' ,'AU/' . $input['state'] . '/' . $input['postcode'] . '/' . $input['suburb'] . '/' . $input['line1']);

		$location = $bing_map_api->get_location_by_address($address);

		$input['event_venue'] = $input['line1'] . ', ' . $input['suburb'] . ', ' . $input['postcode'] . ', ' . $input['state'] . ', AU' ; 


		if($location['lat'] != '' && $location['long'] != ''){
			$distance = $bing_map_api->get_distance(['lat' => SCHOOL_LATITUDE, 'long' => SCHOOL_LONGITUDE], $location);
			$input['distance_from_school'] = $distance['distance'];
			$input['travel_time'] = $distance['travel_time'];
		}
		$input['updated_date'] = date("Y-m-d H:i:s");		
		$update = $this->update($input, 'id', $id);
		return $update;
	}	
}
