<?php

/*This file includes common functions*/


/**
 * Redirect fumction
 *
 * @param   string   $url The redirect url.
 */
function redirect($url){
    header('Location: ' .$url);
}