<?php

if(!defined('ROOT')) exit;

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container-fluid">
		<a class="navbar-brand" href="<?php echo BASE_URL; ?>">Event Management System</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-link" aria-current="page" href="<?php echo BASE_URL . '/event'; ?>">Events</a>
				<a class="nav-link" href="<?php echo BASE_URL . '/organizer'; ?>">Organizers</a>
				<a class="nav-link" href="<?php echo BASE_URL . '/participant_group'; ?>">Participants Group</a>
				<a class="nav-link" href="<?php echo BASE_URL . '/participant'; ?>">Participants</a>
			</div>
		</div>
	</div>
</nav>