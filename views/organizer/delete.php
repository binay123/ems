<?php

/*Organizer Delete Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/organizer');
}else{
	$organizer = new organizer();
	$delete = $organizer->delete($item_id);
	if($delete){
		$result['success'] = true;
		$result['message'] = 'Organizer deleted successfully';
		redirect(BASE_URL . '/organizer');
	}
}