<?php

/*Organizer Index Page*/

if(!defined('ROOT')) exit;


$organizer = new organizer();

$organizers_array = $organizer->index();

include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Organizers</h5>		
		<div class="card-body">
			<a class="btn btn-primary" href="<?php echo BASE_URL . '/organizer/create'; ?>">Add Organizer</a>
			<?php if(!empty($organizers_array)){ ?>
				<table class="table table-striped">
					<thead>
					    <tr>					      
					    	<th scope="col">Name</th>
					      	<th scope="col">Address</th>
					      	<th scope="col">Created At</th>
					      	<th scope="col">Updated At</th>
					      	<th scope="col">Action</th>					      	
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($organizers_array as $o){ ?>
						    <tr>					      
						    	<td><?php echo $o['name']; ?></td>
						      	<td><?php echo $o['address']; ?></td>
						      	<td><?php echo $o['created_date']; ?></td>
						      	<td><?php echo $o['updated_date']; ?></td>
						      	<td>
						      		<a class="btn btn-primary" href="<?php echo BASE_URL . '/organizer/edit/' . $o['id']; ?>">Edit</a>
						      		<a class="btn btn-danger" href="<?php echo BASE_URL . '/organizer/delete/' . $o['id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
						      	</td>
						    </tr>
						<?php } ?>
						    
					  </tbody>
				</table>
			<?php } ?>
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


