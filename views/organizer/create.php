<?php

/*Organizer Create Page*/

if(!defined('ROOT')) exit;

if(isset($_POST['submit'])){

	$input['name'] = isset($_POST['name']) ? $_POST['name'] : '';
	$input['address'] = isset($_POST['address']) ? $_POST['address'] : '';

	if($input['name'] == '' || $input['address'] == ''){
		$result['success'] = false;
		$result['message'] = 'All filelds are required';
	}else{
		$organizer = new organizer();

		$create = $organizer->create($input);

		if($create){
			$result['success'] = true;
			$result['message'] = 'Organizer created successfully';
			redirect(BASE_URL . '/organizer');
		}else{
			$result['success'] = false;
			$result['message'] = 'System error';
		}

	}
}


include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Create Organizer</h5>		
		<div class="card-body">		
			<form method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<?php if(isset($result['success']) && $result['success'] == false){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					 	<?php echo $result['message']; ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php } ?>
				<div class="mb-3">
			    	<label for="name" class="form-label">Name</label>
			    	<input type="text" name="name" value="<?php echo isset($input['name']) ? $input['name'] : '' ; ?>" class="form-control" id="name" required>
			  	</div>
			  	<div class="mb-3">
			    	<label for="address" class="form-label">Address</label>
			    	<input type="text" name="address" value="<?php echo isset($input['address']) ? $input['address'] : '' ; ?>" class="form-control" id="address" required>
			  	</div>			  
			  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
			</form>
			
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


