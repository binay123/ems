<?php

/*Event Index Page*/

if(!defined('ROOT')) exit;

if(isset($_POST['filter_submit'])){

	$input['filter_event_date_from'] = isset($_POST['filter_event_date_from']) ? $_POST['filter_event_date_from'] : '';
	$input['filter_event_date_to'] = isset($_POST['filter_event_date_to']) ? $_POST['filter_event_date_to'] : '';
	$input['filter_event_type'] = isset($_POST['filter_event_type']) ? $_POST['filter_event_type'] : '';
	$input['sort_by'] = isset($_POST['sort_by']) ? $_POST['sort_by'] : '';

	$condition = [];

	$sort_by = '';

	if($input['filter_event_date_from'] != ''){
		$condition['where_min'][0]['column'] = 'event_date';
		$condition['where_min'][0]['value'] = $input['filter_event_date_from'];
	}

	if($input['filter_event_date_to'] != ''){
		$condition['where_max'][0]['column'] = 'event_date';
		$condition['where_max'][0]['value'] = $input['filter_event_date_to'];
	}

	if($input['filter_event_type'] != ''){
		$condition['where'][0]['column'] = 'event_type';
		$condition['where'][0]['value'] = $input['filter_event_type'];
	}

	if($input['sort_by'] != ''){
		$sort_by = str_replace('sort_', '', $input['sort_by']);
	}

	$event = new event();
	$events_array = $event->index($condition, $sort_by);

}else{
	$event = new event();

	$events_array = $event->index();
}




include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="card">
	<h5 class="card-header">Events</h5>		
	<div class="card-body">
		<a class="btn btn-primary" href="<?php echo BASE_URL . '/event/create'; ?>">Add Event</a>
		<form method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
			<div class="mb-3">
		    	<label for="filter_event_date_from" class="form-label">Date From</label>
		    	<input type="date" name="filter_event_date_from" value="<?php echo isset($input['filter_event_date_from']) ? $input['filter_event_date_from'] : '' ; ?>" class="form-control" id="filter_event_date_from">
		  	</div>
		  	<div class="mb-3">
		    	<label for="filter_event_date_to" class="form-label">Date To</label>
		    	<input type="date" name="filter_event_date_to" value="<?php echo isset($input['filter_event_date_to']) ? $input['filter_event_date_to'] : '' ; ?>" class="form-control" id="filter_event_date_to">
		  	</div>
		  	<div class="mb-3">
				<label for="filter_event_type" class="form-label">Type</label>
		    	<select name="filter_event_type" id="filter_event_type" class="form-control">
					<option value="" <?php if(!isset($input['filter_event_type']) || $input['filter_event_type'] == ''){ echo ('selected'); } ?> >&nbsp;</option>
					<option value="excursion" <?php if(isset($input['filter_event_type']) && $input['filter_event_type'] == 'excursion'){ echo ('selected'); } ?> >Excursion</option>	
					<option value="camp" <?php if(isset($input['filter_event_type']) && $input['filter_event_type'] == 'camp'){ echo ('selected'); } ?> >Camp</option>
					<option value="sport" <?php if(isset($input['filter_event_type']) && $input['filter_event_type'] == 'sport'){ echo ('selected'); } ?> >Sport</option>	
					<option value="cocurricular" <?php if(isset($input['filter_event_type']) && $input['filter_event_type'] == 'cocurricular'){ echo ('selected'); } ?> >Co-Curricular</option>							
				</select>
		  	</div>
		  	<div class="mb-3">
				<label for="sort_by" class="form-label">Sort By</label>
		    	<select name="sort_by" id="sort_by" class="form-control">
					<option value="" <?php if(!isset($input['sort_by']) || $input['sort_by'] == ''){ echo ('selected'); } ?> >&nbsp;</option>
					<option value="sort_event_date" <?php if(isset($input['sort_by']) && $input['sort_by'] == 'sort_event_date'){ echo ('selected'); } ?> >Date</option>	
					<option value="sort_event_type" <?php if(isset($input['sort_by']) && $input['sort_by'] == 'sort_event_type'){ echo ('selected'); } ?> >Event Type</option>
					<option value="sort_distance_from_school" <?php if(isset($input['sort_by']) && $input['sort_by'] == 'sort_distance_from_school'){ echo ('selected'); } ?> >Distance From School</option>		
				</select>
		  	</div>
		  	<button type="submit" name="filter_submit" class="btn btn-primary">Filter Events</button>
		</form>
		<?php if(!empty($events_array)){ 
			$organizer = new organizer();
			$participant_group = new participant_group();
			?>
			<table class="table table-striped">
				<thead>
				    <tr>					      
				    	<th scope="col">Name</th>
				      	<th scope="col">Description</th>
				      	<th scope="col">Type</th>
				      	<th scope="col">Event Date</th>
				      	<th scope="col">Organizer</th>
				      	<th scope="col">Participants</th>
				      	<th scope="col">Venue</th>	
				      	<th scope="col">Distance From School</th>
				      	<th scope="col">Travel Time</th>	
				      	<th scope="col">Created Date</th>	
				      	<th scope="col">Updted Date</th>
				      	<th scope="col">Action</th>		      	
				    </tr>
				  </thead>
				  <tbody>
				  	<?php foreach($events_array as $e){ 
				  		$event_organizer_name = '';
				  		$event_participant_group = '';
				  		foreach(explode( ',', $e['event_organizer']) as $organizer_id){
				  			$event_organizer = $organizer->fetch_by_id($organizer_id);
				  			$event_organizer_name .= isset($event_organizer['name']) ? $event_organizer['name'] . ', ' : '';				  				  			
				  		}

				  		foreach(explode( ',', $e['event_participant']) as $group_id){
				  			$event_participant = $participant_group->fetch_by_id($group_id);
				  			$event_participant_group .= isset($event_participant['group_name']) ? $event_participant['group_name'] . ', ' : '';				  				  			
				  		}

				  		?>
					    <tr>					      
					    	<td><?php echo $e['event_name']; ?></td>
					    	<td><?php echo $e['event_description']; ?></td>
					      	<td><?php echo $e['event_type']; ?></td>
					      	<td><?php echo $e['event_date'] . ' ' . $e['event_time']; ?></td>
					      	<td><?php echo rtrim($event_organizer_name, ', '); ?></td>
					      	<td><?php echo rtrim($event_participant_group, ', '); ?></td>
					      	<td><?php echo $e['event_venue']; ?></td>
					      	<td><?php echo round($e['distance_from_school'], 2) . ' km'; ?></td>
					      	<td><?php echo round($e['travel_time']) . ' min'; ?></td>
					      	<td><?php echo $e['created_date']; ?></td>
					      	<td><?php echo $e['updated_date']; ?></td>
					      	<td>
					      		<a class="btn btn-primary" href="<?php echo BASE_URL . '/event/edit/' . $e['id']; ?>">Edit</a>
					      		<a class="btn btn-danger" href="<?php echo BASE_URL . '/event/delete/' . $e['id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
					      		<a class="btn btn-info" href="<?php echo BASE_URL . '/event/participants/' . $e['id']; ?>">List Participants</a>
					      	</td>
					    </tr>
					<?php } ?>
					    
				  </tbody>
			</table>
		<?php } else{ ?>

			<div class="alert alert-warning alert-dismissible fade show" role="alert">
				 	No events for given filters. Try changing the filter values.
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>

		<?php } ?>
	</div>
</div>

	
<?php
include(VIEWS . '/footer.php');
?>