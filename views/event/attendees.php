<?php

/*Event Attendies List*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/event');
}else{

		$event = new event();
		$events_array = $event->fetch_by_id($item_id);
	
		$payment = new payment();
		$attendees = $payment->get_attendees($item_id);	

}

include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Attendees of <?php echo $events_array['event_name']; ?></h5>		
		<div class="card-body">	
			<p>Event Venue : <?php echo $events_array['event_venue'];?> </p>
			<p>Event Date : <?php echo $events_array['event_date'] . ' ' . $events_array['event_time'];?> </p>
			<p>Distance From School : <?php echo round($events_array['distance_from_school'], 2) . ' km';?> </p>
			<p>Travel Time : <?php echo round($events_array['travel_time']) . ' minutes';?> </p>
			<a class="btn btn-primary" href="<?php echo BASE_URL . '/event/participants/' . $item_id; ?>">List of Participants</a>
			<?php if(!empty($attendees)){ ?>
				<table class="table table-striped">
					<thead>
					    <tr>					      
					    	<th scope="col">Group</th>
					      	<th scope="col">First Name</th>
					      	<th scope="col">Last Name</th>				      	
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($attendees as $p){ ?>
						    <tr>					      
						    	<td><?php echo $p['group_name']; ?></td>
						    	<td><?php echo $p['first_name']; ?></td>
						      	<td><?php echo $p['last_name']; ?></td>						      	
						    </tr>
						<?php } ?>
						    
					  </tbody>
				</table>
			<?php }else{ ?>

				<div class="alert alert-warning alert-dismissible fade show" role="alert">
				 	No one has made payment.
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				</div>

			<?php } ?>
		</div>
	</div>
</div>
	

	
<?php
include(VIEWS . '/footer.php');
?>