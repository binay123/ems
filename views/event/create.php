<?php

/*Event Create Page*/

if(!defined('ROOT')) exit;

if(isset($_POST['submit'])){
	$input['event_name'] = isset($_POST['event_name']) ? $_POST['event_name'] : '';
	$input['event_description'] = isset($_POST['event_description']) ? $_POST['event_description'] : '';
	$input['event_type'] = isset($_POST['event_type']) ? $_POST['event_type'] : '';
	$input['event_date'] = isset($_POST['event_date']) ? $_POST['event_date'] : '';
	$input['event_time'] = isset($_POST['event_time']) ? $_POST['event_time'] : '';
	$input['event_organizer'] = isset($_POST['event_organizer']) ? $_POST['event_organizer'] : '';
	$input['event_participant'] = isset($_POST['event_participant']) ? $_POST['event_participant'] : '';
	$input['line1'] = isset($_POST['line1']) ? trim($_POST['line1']) : '';
	$input['suburb'] = isset($_POST['suburb']) ? trim($_POST['suburb']) : '';
	$input['postcode'] = isset($_POST['postcode']) ? trim($_POST['postcode']) : '';
	$input['state'] = isset($_POST['state']) ? trim($_POST['state']) : '';

	if($input['event_name'] == '' || $input['event_description'] == '' || $input['event_type'] == '' || $input['event_date'] == '' || $input['event_time'] == '' || $input['event_organizer'] == '' || $input['event_participant'] == '' || $input['line1'] == '' || $input['suburb'] == '' || $input['postcode'] == '' || $input['state'] == ''){
		$result['success'] = false;
		$result['message'] = 'All filelds are required';
	}else{
		$event = new event();

		$create = $event->create($input);

		if($create){
			$result['success'] = true;
			$result['message'] = 'Event created successfully';
			redirect(BASE_URL . '/event');
		}else{
			$result['success'] = false;
			$result['message'] = 'System error';
		}

	}
}

$participant_group = new participant_group();
$groups = $participant_group->index();

$event_organizers = new organizer();
$organizers = $event_organizers->index();


include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Create Event</h5>		
		<div class="card-body">		
			<form method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<?php if(isset($result['success']) && $result['success'] == false){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					 	<?php echo $result['message']; ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php } ?>
				<div class="mb-3">
			    	<label for="event_name" class="form-label">Name</label>
			    	<input type="text" name="event_name" value="<?php echo isset($input['event_name']) ? $input['event_name'] : '' ; ?>" class="form-control" id="event_name" required>
			  	</div>
			  	<div class="mb-3">
			    	<label for="event_description" class="form-label">Description</label>
			    	<textarea class="form-control" id="event_description" name="event_description"><?php echo isset($input['event_description']) ? $input['event_description'] : '' ; ?></textarea>
			  	</div>
			  	<div class="mb-3">
					<label for="event_type" class="form-label">Type</label>
			    	<select name="event_type" id="event_type" class="form-control" required>
						<option value="" <?php if(!isset($input['event_type']) || $input['event_type'] == ''){ echo ('selected'); } ?> >&nbsp;</option>
						<option value="excursion" <?php if(isset($input['event_type']) && $input['event_type'] == 'excursion'){ echo ('selected'); } ?> >Excursion</option>	
						<option value="camp" <?php if(isset($input['event_type']) && $input['event_type'] == 'camp'){ echo ('selected'); } ?> >Camp</option>
						<option value="sport" <?php if(isset($input['event_type']) && $input['event_type'] == 'sport'){ echo ('selected'); } ?> >Sport</option>	
						<option value="cocurricular" <?php if(isset($input['event_type']) && $input['event_type'] == 'cocurricular'){ echo ('selected'); } ?> >Co-Curricular</option>							
					</select>
			  	</div>
			  	<div class="mb-3">
			    	<label for="event_date" class="form-label">Date</label>
			    	<input type="date" name="event_date" value="<?php echo isset($input['event_date']) ? $input['event_date'] : '' ; ?>" class="form-control" id="event_date" required>
			  	</div>				
				<div class="mb-3">
			    	<label for="event_time" class="form-label">Time</label>
			    	<input type="time" name="event_time" value="<?php echo isset($input['event_time']) ? $input['event_time'] : '' ; ?>" class="form-control" id="event_time" required>
			  	</div>
			  	<div class="mb-3">			  		
			    	<label for="event_organizer" class="form-label">Organizers</label>
			    	<?php foreach($organizers as $organizer){ 
				  			$checked = '';
				  			if(isset($input['event_organizer']) && in_array($organizer, explode(',', $input['event_organizer']))){
				  				$checked = 'checked';
				  			} 
				  	?>
				  	<div class="form-check">
						<input class="form-check-input" type="checkbox" name="event_organizer[]" value="<?php echo isset($organizer['id']) ? $organizer['id'] : '' ; ?>" id="<?php echo isset($organizer['id']) ? $organizer['id'] : '' ; ?>"> <?php echo $checked; ?>
					  	<label class="form-check-label" for="<?php echo isset($organizer['id']) ? $organizer['id'] : '' ; ?>"><?php echo isset($organizer['name']) ? $organizer['name'] : '' ; ?></label>
					</div>
					<?php } ?>			    	
			  	</div>	
			  	<div class="mb-3">
			    	<label for="event_participant" class="form-label">Participant</label>
			    	<?php foreach($groups as $participant_group){ 
				  			$checked = '';
				  			if(isset($input['event_participant']) && in_array($participant_group, explode(',', $input['event_participant']))){
				  				$checked = 'checked';
				  			} 
				  	?>
				  	<div class="form-check">
						<input class="form-check-input" type="checkbox" name="event_participant[]" value="<?php echo isset($participant_group['id']) ? $participant_group['id'] : '' ; ?>" id="<?php echo isset($participant_group['id']) ? $participant_group['id'] : '' ; ?>"> <?php echo $checked; ?>
					  	<label class="form-check-label" for="<?php echo isset($participant_group['id']) ? $participant_group['id'] : '' ; ?>"><?php echo isset($participant_group['group_name']) ? $participant_group['group_name'] : '' ; ?></label>
					</div>
					<?php } ?>	
			  	</div>
			  	<label for="venue" class="form-label">Venue</label>	
			  	<div class="mb-3">			  		
			    	<label for="line1" class="form-label">Address Line1</label>
			    	<input type="text" name="line1" value="<?php echo isset($input['line1']) ? $input['line1'] : '' ; ?>" class="form-control" id="line1" required>
			  	</div>	 
			  	<div class="mb-3">			  		
			    	<label for="suburb" class="form-label">Suburb</label>
			    	<input type="text" name="suburb" value="<?php echo isset($input['suburb']) ? $input['suburb'] : '' ; ?>" class="form-control" id="suburb" required>
			  	</div> 
			  	<div class="mb-3">			  		
			    	<label for="postcode" class="form-label">Postcode</label>
			    	<input type="number" name="postcode" value="<?php echo isset($input['postcode']) ? $input['postcode'] : '' ; ?>" class="form-control" id="postcode" required>
			  	</div> 
			  	<div class="mb-3">			  		
			    	<label for="state" class="form-label">State</label>
			    	<input type="text" name="state" value="<?php echo isset($input['state']) ? $input['state'] : '' ; ?>" class="form-control" id="state" required>
			  	</div>
			  	<button type="submit" name="submit" class="btn btn-primary">Submit</button>
			</form>
			
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


