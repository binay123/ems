<?php

/*Event Participants List*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/event');
}else{
	if(isset($_GET['action']) && $_GET['action'] == 'receive_payment' && isset($_GET['participant_id'])){
		$input['event_id'] = $item_id;
		$input['participant_id'] = $_GET['participant_id'];

		$payment = new payment();
		if($payment->create($input)){
			redirect(BASE_URL . '/event/participants/' . $item_id);
		}

	}else{
		$event = new event();
		$events_array = $event->fetch_by_id($item_id);

		$participant = new participant();
		$participants_array = $participant->index(['where_in' => [['column' => 'group_id', 'value' => $events_array['event_participant']]]]);

		$payment = new payment();
		$payment_array = $payment->fetch_all([], ['where' => [['column' => 'event_id', 'value' => $item_id]]]);
		$payment_user_ids = array_column($payment_array, 'participant_id');		
	}

}

include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Participants of <?php echo $events_array['event_name']; ?></h5>		
		<div class="card-body">	
			<p>Event Venue : <?php echo $events_array['event_venue'];?> </p>
			<p>Event Date : <?php echo $events_array['event_date'] . ' ' . $events_array['event_time'];?> </p>
			<p>Distance From School : <?php echo round($events_array['distance_from_school'], 2) . ' km';?> </p>
			<p>Travel Time : <?php echo round($events_array['travel_time']) . ' minutes';?> </p>
			<a class="btn btn-primary" href="<?php echo BASE_URL . '/event/attendees/' . $item_id; ?>">List of Attendees</a>
			<?php if(!empty($participants_array)){ ?>
				<table class="table table-striped">
					<thead>
					    <tr>					      
					    	<th scope="col">Group</th>
					      	<th scope="col">First Name</th>
					      	<th scope="col">Last Name</th>
					      	<th scope="col">Action</th>					      	
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($participants_array as $p){ ?>
						    <tr>					      
						    	<td><?php echo $p['group_name']; ?></td>
						    	<td><?php echo $p['first_name']; ?></td>
						      	<td><?php echo $p['last_name']; ?></td>
						      	<td>
						      		<?php if(in_array($p['id'], $payment_user_ids)){ ?>
						      				<button class="btn btn-success">Paid</button>
						      		<?php }else{ ?>
						      				<a class="btn btn-primary" href="<?php echo BASE_URL . '/event/participants/' . $item_id . '/&action=receive_payment&participant_id=' . $p['id']; ?>" onclick="return confirm('Are you sure you have received payment?');">Receive Payment</a>
						      		<?php } ?>
						      		
						      	</td>
						    </tr>
						<?php } ?>
						    
					  </tbody>
				</table>
			<?php } ?>
		</div>
	</div>
</div>
	

	
<?php
include(VIEWS . '/footer.php');
?>