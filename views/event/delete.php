<?php

/*Event Delete Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/event');
}else{
	$event = new event();
	$delete = $event->delete($item_id);
	if($delete){
		$result['success'] = true;
		$result['message'] = 'Event deleted successfully';
		redirect(BASE_URL . '/event');
	}
}