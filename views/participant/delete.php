<?php

/*Participant Delete Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/participant');
}else{
	$participant = new participant();
	$delete = $participant->delete($item_id);
	if($delete){
		$result['success'] = true;
		$result['message'] = 'Participant deleted successfully';
		redirect(BASE_URL . '/participant');
	}
}