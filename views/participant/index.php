<?php

/*Participant Index Page*/

if(!defined('ROOT')) exit;


$participant = new participant();

$participants_array = $participant->index();

include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Participants</h5>		
		<div class="card-body">
			<a class="btn btn-primary" href="<?php echo BASE_URL . '/participant/create'; ?>">Add Participant</a>
			<?php if(!empty($participants_array)){ ?>
				<table class="table table-striped">
					<thead>
					    <tr>					      
					    	<th scope="col">Group</th>
					      	<th scope="col">First Name</th>
					      	<th scope="col">Last Name</th>
					      	<th scope="col">Created At</th>
					      	<th scope="col">Updated At</th>
					      	<th scope="col">Action</th>					      	
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($participants_array as $p){ ?>
						    <tr>					      
						    	<td><?php echo $p['group_name']; ?></td>
						    	<td><?php echo $p['first_name']; ?></td>
						      	<td><?php echo $p['last_name']; ?></td>
						      	<td><?php echo $p['created_date']; ?></td>
						      	<td><?php echo $p['updated_date']; ?></td>
						      	<td>
						      		<a class="btn btn-primary" href="<?php echo BASE_URL . '/participant/edit/' . $p['id']; ?>">Edit</a>
						      		<a class="btn btn-danger" href="<?php echo BASE_URL . '/participant/delete/' . $p['id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
						      	</td>
						    </tr>
						<?php } ?>
						    
					  </tbody>
				</table>
			<?php } ?>
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


