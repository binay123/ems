<?php

/*Participant Edit Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/participant');
}else{
	$participant = new participant();
	$input = $participant->fetch_by_id($item_id);
}

if(isset($_POST['submit'])){
	$id = isset($_POST['id']) ? $_POST['id'] : '';
	$input['group_id'] = isset($_POST['group_id']) ? $_POST['group_id'] : '';
	$input['first_name'] = isset($_POST['first_name']) ? $_POST['first_name'] : '';
	$input['last_name'] = isset($_POST['last_name']) ? $_POST['last_name'] : '';

	if($id == ''){
		$result['success'] = false;
		$result['message'] = 'This action cannot be performed.';
	}elseif($input['group_id'] == '' || $input['first_name'] == '' || $input['last_name'] == ''){
		$result['success'] = false;
		$result['message'] = 'All filelds are required';
	}else{
		$participant = new participant();

		$update = $participant->edit($input, $id);

		if($update){
			$result['success'] = true;
			$result['message'] = 'Participant edited successfully';
			redirect(BASE_URL . '/participant');
		}else{
			$result['success'] = false;
			$result['message'] = 'System error';
		}

	}
}

$participant_group = new participant_group();
$groups = $participant_group->index();


include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Edit Participant</h5>		
		<div class="card-body">		
			<form method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<?php if(isset($result['success']) && $result['success'] == false){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					 	<?php echo $result['message']; ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php } ?>
				<input type="hidden" name="id" value="<?php echo isset($item_id) ? $item_id : '' ; ?>" class="form-control" id="id">
				<div class="mb-3">
					<label for="group_id" class="form-label">Group</label>
			    	<select name="group_id" id="group_id" class="form-control" required>
						<option value="" <?php if(!isset($input['group_id']) || $input['group_id'] == ''){ echo ('selected'); } ?> >&nbsp;</option>
						<?php foreach($groups as $g){ ?>
							<option value="<?php echo $g['id']; ?>" <?php if(isset($input['group_id']) && $input['group_id'] == $g['id']){ echo ('selected'); } ?> ><?php echo $g['group_name']; ?></option>
						<?php }	?>	
									
					</select>
			  	</div>
				<div class="mb-3">
			    	<label for="first_name" class="form-label">First Name</label>
			    	<input type="text" name="first_name" value="<?php echo isset($input['first_name']) ? $input['first_name'] : '' ; ?>" class="form-control" id="first_name" required>
			  	</div>
			  	<div class="mb-3">
			    	<label for="last_name" class="form-label">Last Name</label>
			    	<input type="text" name="last_name" value="<?php echo isset($input['last_name']) ? $input['last_name'] : '' ; ?>" class="form-control" id="last_name" required>
			  	</div>			  
			  	<button type="submit" name="submit" class="btn btn-primary">Update</button>
			</form>
			
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


