<?php

/*Participant Group Index Page*/

if(!defined('ROOT')) exit;


$participant_group = new participant_group();

$participant_groups_array = $participant_group->index();

include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Participant Groups</h5>		
		<div class="card-body">
			<a class="btn btn-primary" href="<?php echo BASE_URL . '/participant_group/create'; ?>">Add Participant Group</a>
			<?php if(!empty($participant_groups_array)){ ?>
				<table class="table table-striped">
					<thead>
					    <tr>					      
					    	<th scope="col">Name</th>
					      	<th scope="col">Description</th>
					      	<th scope="col">Created At</th>
					      	<th scope="col">Updated At</th>
					      	<th scope="col">Action</th>					      	
					    </tr>
					  </thead>
					  <tbody>
					  	<?php foreach($participant_groups_array as $pg){ ?>
						    <tr>					      
						    	<td><?php echo $pg['group_name']; ?></td>
						      	<td><?php echo $pg['group_description']; ?></td>
						      	<td><?php echo $pg['created_date']; ?></td>
						      	<td><?php echo $pg['updated_date']; ?></td>
						      	<td>
						      		<a class="btn btn-primary" href="<?php echo BASE_URL . '/participant_group/edit/' . $pg['id']; ?>">Edit</a>
						      		<a class="btn btn-danger" href="<?php echo BASE_URL . '/participant_group/delete/' . $pg['id']; ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
						      	</td>
						    </tr>
						<?php } ?>
						    
					  </tbody>
				</table>
			<?php } ?>
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


