<?php

/*Organizer Edit Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/participant_group');
}else{
	$participant_group = new participant_group();
	$input = $participant_group->fetch_by_id($item_id);
}


if(isset($_POST['submit'])){

	$id = isset($_POST['id']) ? $_POST['id'] : '';
	$input['group_name'] = isset($_POST['group_name']) ? $_POST['group_name'] : '';
	$input['group_description'] = isset($_POST['group_description']) ? $_POST['group_description'] : '';

	if($id == ''){
		$result['success'] = false;
		$result['message'] = 'This action cannot be performed.';
	}elseif($input['group_name'] == '' || $input['group_description'] == ''){
		$result['success'] = false;
		$result['message'] = 'All filelds are required';
	}else{
		$participant_group = new participant_group();

		$update = $participant_group->edit($input, $id);

		if($update){
			$result['success'] = true;
			$result['message'] = 'Participant Group edited successfully';
			redirect(BASE_URL . '/participant_group');
		}else{
			$result['success'] = false;
			$result['message'] = 'System error';
		}

	}
}


include(VIEWS . '/header.php');
include(VIEWS . '/navbar.php');
?>
<div class="container">
	<div class="card">
		<h5 class="card-header">Edit Participant Group</h5>		
		<div class="card-body">		
			<form method="POST" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<?php if(isset($result['success']) && $result['success'] == false){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					 	<?php echo $result['message']; ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php } ?>
				<input type="hidden" name="id" value="<?php echo isset($item_id) ? $item_id : '' ; ?>" class="form-control" id="id">
				<div class="mb-3">
			    	<label for="group_name" class="form-label">Group Name</label>
			    	<input type="text" name="group_name" value="<?php echo isset($input['group_name']) ? $input['group_name'] : '' ; ?>" class="form-control" id="group_name" required>
			  	</div>
			  	<div class="mb-3">
			    	<label for="group_description" class="form-label">Group Description</label>
			    	<input type="text" name="group_description" value="<?php echo isset($input['group_description']) ? $input['group_description'] : '' ; ?>" class="form-control" id="group_description" required>
			  	</div>			  
			  	<button type="submit" name="submit" class="btn btn-primary">Update</button>
			</form>
			
		</div>
	</div>
</div>
	
<?php
include(VIEWS . '/footer.php');
?>


