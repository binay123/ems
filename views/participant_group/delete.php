<?php

/*Participant Group Delete Page*/

if(!defined('ROOT')) exit;

if($item_id == ''){
	redirect(BASE_URL . '/participant_group');
}else{
	$participant_group = new participant_group();
	$delete = $participant_group->delete($item_id);
	if($delete){
		$result['success'] = true;
		$result['message'] = 'Participant Group deleted successfully';
		redirect(BASE_URL . '/participant_group');
	}
}