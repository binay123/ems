<?php

require_once 'system/constants.php';
require_once SYSTEM . '/functions/core.php';
require_once SYSTEM . '/autoload.php';

//Routing - ALl the request are redirected to index file through htaccess

$input_url = isset($_GET['url']) ? $_GET['url'] : '';

if($input_url != ''){
	$url = strtolower($input_url);
	$url_parts = explode('/', $url);
	$action = isset($url_parts[0]) ? $url_parts[0] : ''; 
	$module = isset($url_parts[1]) ? $url_parts[1] : '';	
	$item_id = isset($url_parts[2]) ? $url_parts[2] : '';

	try{

			if($module == ''){
				if(!file_exists(VIEWS . '/' . $action . '/index.php')){
					throw new \Exception('The theme action file "' . VIEWS . '/' . $action . '/index.php' . '" could not be found.');	
				}else{
					include(VIEWS . '/' . $action . '/index.php');
				}		
					
			}else{
				if(!file_exists(VIEWS . '/' . $action . '/' . $module . '.php')){
					throw new \Exception('The theme action file "' . VIEWS . '/' . $action . '/' . $module . '.php' . '" could not be found.');	
				}else{
					include(VIEWS . '/' . $action . '/' . $module . '.php');
				}
			}
		}catch(\Exception $e){
			echo $e->getMessage();
		}			
	
}else{
	include(VIEWS . '/event/index.php');
}



?>