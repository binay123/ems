-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2022 at 11:51 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_description` text NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_organizer` varchar(255) NOT NULL,
  `event_participant` varchar(255) NOT NULL,
  `event_venue` varchar(255) NOT NULL,
  `line1` varchar(255) NOT NULL,
  `suburb` varchar(255) NOT NULL,
  `postcode` int(11) NOT NULL,
  `state` varchar(255) NOT NULL,
  `distance_from_school` float DEFAULT NULL,
  `travel_time` float DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_name`, `event_description`, `event_type`, `event_date`, `event_time`, `event_organizer`, `event_participant`, `event_venue`, `line1`, `suburb`, `postcode`, `state`, `distance_from_school`, `travel_time`, `created_date`, `updated_date`, `flag`) VALUES
(1, 'Cricket', 'School Cricket Event', 'sport', '2022-01-19', '17:46:00', '5', '1,3', '75 Durham St, Hurstville, 2220, NSW, AU', '75 Durham St', 'Hurstville', 2220, 'NSW', 0.397, 0.8667, '2022-01-16 09:05:49', '2022-01-16 22:48:50', 1),
(3, 'Tour', 'Educational Tour', 'excursion', '2022-01-23', '19:46:00', '5,6', '1', 'Cliff Dr, Katoomba, 2780, NSW, AU', 'Cliff Dr', 'Katoomba', 2780, 'NSW', 107.751, 84.5167, '2022-01-16 09:54:39', '2022-01-16 09:54:39', 1),
(4, 'Camp', 'Camping', 'camp', '2022-01-30', '20:17:00', '4', '1', 'Pines, Martinsville, 2265, NSW, AU', 'Pines', 'Martinsville', 2265, 'NSW', 133.376, 106.617, '2022-01-16 10:15:31', '2022-01-16 22:54:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizer`
--

CREATE TABLE `organizer` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizer`
--

INSERT INTO `organizer` (`id`, `name`, `address`, `created_date`, `updated_date`, `flag`) VALUES
(2, 'King Foundation', '123 Turrella Street 2205 NSW', '2022-01-16 02:58:02', '2022-01-16 23:50:20', 1),
(4, 'Dale Group', '51 St Georges Pde Hurstville 2220 NSW', '2022-01-16 03:00:10', '2022-01-16 23:50:38', 1),
(5, 'Sydney Cricket Council', 'Moore Park, NSW', '2022-01-16 03:30:34', '2022-01-16 22:53:05', 1),
(6, 'NRNA', 'Rockdale, NSW', '2022-01-16 06:13:09', '2022-01-16 22:53:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `group_id`, `first_name`, `last_name`, `created_date`, `updated_date`, `flag`) VALUES
(1, 3, 'Ram', 'Gurung', '2022-01-16 04:26:22', '2022-01-16 04:26:22', 1),
(2, 1, 'Binay', 'Subedi', '2022-01-16 04:45:12', '2022-01-16 13:49:22', 1),
(3, 1, 'Susmu', 'Lc', '2022-01-16 10:49:23', '2022-01-16 10:49:23', 1),
(4, 1, 'Joel', 'Vasq', '2022-01-16 10:49:40', '2022-01-16 10:49:40', 1),
(5, 2, 'Jack', 'Mitch', '2022-01-16 10:53:36', '2022-01-16 10:53:36', 1),
(6, 3, 'John', 'Dion', '2022-01-16 22:49:23', '2022-01-16 22:49:23', 1),
(7, 3, 'Raju', 'Khanal', '2022-01-16 22:49:42', '2022-01-16 22:49:42', 1),
(8, 2, 'Taylor', 'Mitchel', '2022-01-16 22:49:58', '2022-01-16 22:49:58', 1),
(9, 3, 'Michael', 'Dale', '2022-01-16 22:50:10', '2022-01-16 22:50:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `participant_group`
--

CREATE TABLE `participant_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_description` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participant_group`
--

INSERT INTO `participant_group` (`id`, `group_name`, `group_description`, `created_date`, `updated_date`, `flag`) VALUES
(1, 'Student', 'Student Group Update', '2022-01-16 03:40:44', '2022-01-16 03:58:43', 1),
(2, 'Parents', 'Parents Group', '2022-01-16 03:41:01', '2022-01-16 03:41:01', 1),
(3, 'Volunteer', 'Volunteer Group', '2022-01-16 03:41:17', '2022-01-16 03:41:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `flag` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `event_id`, `participant_id`, `created_date`, `flag`) VALUES
(2, 3, 3, '2022-01-16 12:08:12', 1),
(3, 3, 4, '2022-01-16 12:23:44', 1),
(4, 1, 1, '2022-01-16 13:50:03', 1),
(5, 1, 9, '2022-01-16 23:50:06', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizer`
--
ALTER TABLE `organizer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant_group`
--
ALTER TABLE `participant_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `organizer`
--
ALTER TABLE `organizer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `participant_group`
--
ALTER TABLE `participant_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
